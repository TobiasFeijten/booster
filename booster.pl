#! /usr/bin/perl -w

use strict;
use JSON;
use LWP::UserAgent;

die "Please provide four digit year." unless ( defined  $ARGV[0] and $ARGV[0] =~ m/^\d{4}$/ );
my $year = $ARGV[0];

my $ua = new LWP::UserAgent;
$ua->agent("Mozilla/4.0 (compatible; MSIE 4.01; Windows 98)");

my $json = $ua->get("https://user-api.coronatest.nl/vaccinatie/programma/booster/$year/NEE")->content;
die "Failed to get answer from the server.\n" unless defined $json;

my $success = decode_json($json)->{'success'};
die "Unexpected response from the server.\n" unless defined $success;

if ( $success ) {
  print "Yes, people born in $year can get a booster.\n";
} else {
  print "Nope not yet. Check again later but not to quick/often. ;)\n";
}
